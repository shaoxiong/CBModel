# CBModel

[![Gitter](https://badges.gitter.im/Join Chat.svg)](https://gitter.im/cbangchen/CBModel#)
[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=102)](https://opensource.org/licenses/mit-license.php)
[![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.svg?v=102)](https://github.com/ellerbrock/open-source-badge/)  

FMDB encapsulation,Model storage,include Model Maker!

## Installation with CocoaPods

CocoaPods is a dependency manager for Objective-C

### Podfile

```
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '7.0'

pod 'CBModel', '~> 1.0.1'
```

Then, **cd** you directory and  run the following command:

```
$ pod install
```
--- 
### Table Manager

##### Clear Table

```
+ (BOOL)clearTable;
```
--- 
### Save Methods

##### Save Single Object

```
- (BOOL)saveSingleObject;
```
##### Save Single Object Asynchronously

```
- (void)AsySaveSingleObjectsWithResultBlock:(CBAsyResultBlock)resultBock;
```

##### Save Objects With Arrary

```
+ (BOOL)saveObjectsWithArray:(NSArray *)array;
```

##### Save Objects With Arrary Asynchronously

```
+ (void)AsySaveObjectsWithArray:(NSArray *)array
                    resultBlock:(CBAsyResultBlock)resultBock;
```
--- 

### Update Methods


##### Update Single Object

```
- (BOOL)updateSingleObject;
```

##### Update Single Object Asynchronously

```
- (void)AsyUpdateSingleObjectsWithResultBlock:(CBAsyResultBlock)resultBock;
```

##### Update Objects With Array

```
+ (BOOL)updateObjectsWithArray:(NSArray *)array;
```

##### Update Objects With Array Asynchronously

```
+ (void)AsyUpdateObjectsWithArray:(NSArray *)array
                      resultBlock:(CBAsyResultBlock)resultBock;
```
--- 

### Delete Methods

##### Delete Single Object

```
- (BOOL)deleteSingleObject;
```
##### Delete Single Object Asynchronously

```
- (void)AsyDeleteSingleObjectsWithResultBlock:(CBAsyResultBlock)resultBock;
```

##### Delete Objects With Array

```
+ (BOOL)deleteObjectsWithArray:(NSArray *)array;
```

##### Delete Objects With Array Asynchronously

```
+ (void)AsyDeleteObjectsWithArray:(NSArray *)array
                      resultBlock:(CBAsyResultBlock)resultBock;
```

##### Delete Objects With Criteria

```
+ (BOOL)deleteObjectsWithCriteria:(NSString *)criteria;
```
##### Delete Objects With Muti Criterias

```
+ (BOOL)deleteObjectsWithFormat:(NSString *)format, ...;
```
--- 

### Find Methods

##### Find All Objects

```
+ (NSArray *)findAllObjects;
```

##### Find Objects With Criteria

```
+ (NSArray *)findByCriteria:(NSString *)criteria;
```
##### Find Objects With Muti Criterias

```
+ (NSArray *)findWithFormat:(NSString *)format, ...;
```
--- 
### Make The Model

##### Dictionary -> Model

```
- (void)configurePropertyWithDictionary:(NSDictionary *)dictionary;
```

##### Dictionary Include Model Object -> Model

```
- (NSArray *)configurePropertyWithArray:(NSArray *)array;
```

##### Model -> Dictionary

```
- (NSDictionary *)makeDictionary;
```
